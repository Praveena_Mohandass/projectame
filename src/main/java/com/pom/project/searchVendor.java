package com.pom.project;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class searchVendor extends loginPage {
	
	public searchVendor() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID,using="vendorTaxID") WebElement taxId;
	@FindBy(how = How.ID,using="buttonSearch") WebElement buttonSearch;
	
	@Test
	public searchVendor enterTaxid() {
		
		taxId.sendKeys("FR453231");
		
		return this;
	}
	@Test
	public searchResultPage enterSearch() {
		
		buttonSearch.click();
		
		return new searchResultPage();
	}
	
}
