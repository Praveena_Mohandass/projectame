package com.pom.project;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

public class searchResultPage extends loginPage {
	
	
	public searchResultPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//td[text()='Minitronic Systems']") WebElement vendorName;

	@Test
	public void printVendorName() {
		System.out.println(vendorName.getText());
		
	}
	
	
}
