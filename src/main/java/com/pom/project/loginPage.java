package com.pom.project;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class loginPage {
	
	public static ChromeDriver driver;

	public loginPage() {
		PageFactory.initElements(driver, this);
		}
	@FindBy(how=How.ID,using="email") WebElement email;
	@FindBy(how=How.ID,using="password") WebElement password;
	@FindBy(how=How.ID,using="buttonLogin") WebElement loginButton;
	
	@BeforeSuite
	public void enterUrl() {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		
		driver = new ChromeDriver();
		
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		
		driver.get("https://acme-test.uipath.com/account/login");
	}
	
	
	public loginPage enterUsername() {
		email.sendKeys("praveena2402@gmail.com");
	return this;
	}
	
	public loginPage enterPassword() {
		
		password.sendKeys("abc123");
		
	return this;
	}
	
	public dashBoardPage enterLogin() {
		
		loginButton.click();
		
		return new dashBoardPage();
	}

}
