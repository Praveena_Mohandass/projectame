package com.pom.project;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.testng.annotations.Test;

import net.bytebuddy.dynamic.DynamicType.Builder;

public class dashBoardPage extends loginPage{
	public Actions Builder;
	public dashBoardPage() {
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using="//i[@class='fa fa-truck']") WebElement vendor;	
	@FindBy(how = How.XPATH,using="//a[text()='Search for Vendor']") WebElement searchvendor;
	
	
	public dashBoardPage hoverVendor() {
		
		Builder = new Actions(driver);
		
		Builder.moveToElement(vendor).perform();
		 return this;
	}
	
	public searchVendor searchVendor() {
		
		Builder.click(searchvendor).perform();
		
		return new searchVendor();
	}
	}
